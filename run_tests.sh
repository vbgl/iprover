#!/bin/bash

# part of the iProver project

#set -x

# test description file should have csv with "," delimiter and the following structure

# test_file,exec_name,exec_options,pass_string,test_goal

# test is failed if either return status is !=0 or the pass_string is not in the output


DESCR_FILE="build_tests.csv"
TEST_DIR="TESTS"

SCRIPT_NAME=$(basename "$0")

usage_msg="$SCRIPT_NAME [-h] [-p PATH] FILE.csv

-h      help
-p      path to test files
        default: TESTS

FILE.csv is the file with the test description in the form:

test_file,exec_name,exec_options,pass_string,test_goal

        default: build_tests.csv
"

#if [[ $# -eq 0 ]]
#then
#    echo "$usage_msg"
#    exit 1
#fi

# if option -t 300 is supplied
while getopts ":h:p:" option; do      
    case "${option}" 
    in
        h)  echo "$usage_msg" 
            exit 1
            ;;
        p) TEST_DIR=${OPTARG}
           ;;
        ?) echo "Error: unsupported options"
           echo ""
           echo "$usage_msg"
           exit 1
        ;;
    esac    
done

# remaining args
shift $(($OPTIND - 1))
if [[ "$@" != "" ]] ; then
    DESCR_FILE="$@"
fi

        
NUM_PASS=0
NUM_FAILED=0
TESTN=0

echo ""
echo "====== runing tests in ${DESCR_FILE}"
echo ""

#cat $DESCR_FILE | while read line; do
while read line; do
    if [[ ${line:0:1} == "#" ]] ; then
        continue # skip comments 
    fi
    TESTN=$((TESTN+1))
    test_file="$(echo "$line" | awk -F, '{print $1}')"
    exec_name="$(echo "$line" | awk -F, '{print $2}')"
    exec_options="$(echo "$line" | awk -F, '{print $3}')"
    pass_string="$(echo "$line" | awk -F, '{print $4}')"
    test_goal="$(echo "$line" | awk -F, '{print $5}')"
    #
    #some annoying empty lines in sterr directed to /dev/null
    ./${exec_name} ${exec_options} $TEST_DIR/${test_file}  2> /dev/null  | grep -q "$pass_string"
    test_exit_status=$?
    #   
    if [ $test_exit_status -eq 0 ]; then
        result="PASS"
        NUM_PASS=$((NUM_PASS+1))
    else
        result="FAILED"
        NUM_FAILED=$((NUM_FAILED+1))
    fi
    echo "$TESTN. ${test_file}: ${test_goal}: ${result}"    
done < $DESCR_FILE 

echo
echo "================"
echo "PASSED: $NUM_PASS"
echo "FAILED: $NUM_FAILED"

if [ $NUM_FAILED -eq 0 ]
then
    exit 0
else
    exit 1
fi
